import scipy.special
from matplotlib.ticker import MaxNLocator
import numpy as np

def typical_p(x): return np.exp(-scipy.special.entr(x).sum(axis=1))
def inverse_typical_p(x): return 1/typical_p(x)
def num_above_typical_p(x):
    return np.sum(x >= typical_p(x)[:, np.newaxis], axis=1)
def mean_p(x, exponent=1):
    nb_array = np.arange(1, x.shape[1]+1).reshape((1, -1))**exponent
    return (x*nb_array).sum(axis=1)

class Score:
    def preprocess(self, x):
        return np.sort(x)[:, ::-1]
    def __call__(self, x):
        x = self.preprocess(x)
        return self.act(x)

class MulMean(Score):
    def __init__(self, mul_factor, exponent=1):
        self.mul_factor = mul_factor
        self.exponent = exponent
    def act(self, x):
        return (mean_p(x, exponent=self.exponent)**(1/self.exponent))*self.mul_factor
    def __str__(self):
        return "<score: mean times {}>".format(self.mul_factor)

class Above(Score):
    def __init__(self, max_w):
        self.max_w = max_w
    def act(self, x):
        return np.sum(x >= self.max_w, axis=1)
    def __str__(self):
        return "<score: above {}>".format(self.max_w)

class SumAbove(Score):
    def __init__(self, max_w):
        self.max_w = max_w
    def act(self, x):
        cum_sum_x = np.cumsum(x, axis=1)
        return np.sum(cum_sum_x <= self.max_w, axis=1)
    def __str__(self):
        return "<score: sum above {}>".format(self.max_w)

def scores(name, config=None):
    if name == 'inverse_typical_prob': #Paper: Total num of interacting nb
        return inverse_typical_p
    elif name == 'num_above_typical_prob': #Paper: Num of important interacting nb
        return num_above_typical_p
    elif name == 'above':
        return Above(config['max_w'])
    elif name == 'sum_above':
        return SumAbove(config['max_w'])
    elif name == 'mean':
        return MulMean(config['mul_factor'], exponent = config['exponent'])
    else: raise Exception('No score named ', name)

def calculate_nb(properties, measure=0, config={}):
    attention = properties["attention_layer"]
    #print(attention.sum(axis=1).min(), attention.sum(axis=1).max())
    return scores(measure, config=config)(attention)

def nb_in_zone(properties, max_r, max_nb, attention_radius=0):
    social = properties['social'][:, 1:(max_nb+1)]
    distance_to_nb = np.sqrt(social[..., 0]**2 + social[..., 1]**2)
    nb_in_attention = (distance_to_nb < attention_radius).sum(axis=1)
    nb_in_radius = (distance_to_nb < max_r).sum(axis=1)
    nb = (nb_in_attention > 0)*nb_in_attention + (nb_in_attention ==0)*nb_in_radius
    return nb

def sum_score_nb(properties):
    attention = properties["attention_layer"]
    return attention.mean(axis=0)

def plot_num_nb_histogram(ax_hist, num_nb, h_max=None, h_min=None):
    if h_max is None: h_max = num_nb.max()
    if h_min is None: h_min = num_nb.min()
    bins = np.arange(h_min, h_max + 2, 1) - .5
    ax_hist.hist(num_nb, density=True, bins=bins, rwidth=0.6)
    ax_hist.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax_hist.get_yaxis().set_ticks([])
    ax_hist.set_xlabel('')

def plot_num_nb_histogram_empty(ax, num_nb, h_max=None, h_min=None,
                                colors=None, line_styles=None,
                                bins=None, bin_width=1):
    if h_max is None: h_max = np.ceil(num_nb.max())
    if h_min is None: h_min = np.floor(num_nb.min())
    if bins is None: bins = np.arange(h_min, h_max + 2, bin_width) - .5
    ax.hist(num_nb, density=True, bins=bins, rwidth=0.6, color=colors,
            histtype='step', ls=line_styles, linewidth=2)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel('Typical number of neighbours')
    ax.set_ylabel('PDF')


### BELOW ARE UNUSED AT THE MOMENT
def plot_num_nb_histogram_empty_line(ax, num_nb, h_max=None, h_min=None,
                                     colors=None, line_styles=None,
                                     bins=None):
    if h_max is None: h_max = num_nb.max()
    if h_min is None: h_min = num_nb.min()
    if bins is None:
        bins = np.linspace(h_min, h_max + 2, 200)
    h, e = np.histogram(num_nb, density=True, bins=bins)
    ax.plot(e[1:]+np.diff(e)[0], h, color=colors, ls=line_styles, linewidth=3, alpha=.8)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel('Typical number of neighbours')
    ax.set_ylabel('PDF')

def plot_sum_score_nb(ax, summed_weight):
    ax.plot(summed_weight)
    summed_weight = summed_weight[np.newaxis,:]/summed_weight.sum()
    s = [inverse_typical_p, num_above_typical_p, MulMean(2),
         scores('sum_above', {'max_w': 0.5}), scores('sum_above', {'max_w': 0.95})]
    color = list('rbkgg')
    for s_, color_ in zip(s, color):
        print(s_, color_)
        ax.axvline(s_(summed_weight), color=color_)
