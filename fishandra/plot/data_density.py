import os
import numpy as np
import matplotlib.pyplot as plt

from .utils import load_model_and_data, _social_and_focal_numpy, \
                restrict_variables_for_attention_numpy, get_multi_histogram
from .new_constants import constants as cons
from .. import loader_constants as loader_cons


def plot_multidim_histogram(h, e, BINS, RANGE, vmin, vmax, cbar_label='',
                            title=''):

    fig, ax_arr = plt.subplots(BINS[0], BINS[3], sharex=True, sharey=True, figsize=(20, 10))
    fig.suptitle(title)
    for i in range(BINS[0]): # focal_vel
        for j in range(BINS[3]): # neighb_vel
            ax = ax_arr[BINS[0] - 1 - i, j]
            im = ax.imshow(h[i, :, :, j].T, vmin=vmin, vmax=vmax,
                           origin='lower',
                           extent=(RANGE[1][0]*cons.DELTA_X, RANGE[1][1]*cons.DELTA_X,
                                   RANGE[2][0]*cons.DELTA_X, RANGE[2][1]*cons.DELTA_X),
                           aspect='equal')
            if i==0:
                ax.set_xlabel(r"$x_i$ (BL)" + "\n" +
                              r"v_i " + "= {0:.2f} - {1:.2f} BL/s".format(e[3][j]*cons.DELTA_V,
                                                              e[3][j+1]*cons.DELTA_V)
                              )
            if j==0:
                ax.set_ylabel(r"v " +"= {0:.2f} - {1:.2f} BL/s".format(e[0][i]*cons.DELTA_V,
                                                                      e[0][i+1]*cons.DELTA_V) +
                              "\n" + r"$y_i$ (BL)")
            ax.set_aspect('equal')

    im.cmap.set_under("w")
    cbar = fig.colorbar(im, ax=ax_arr.ravel().tolist(), label=cbar_label)
    return fig


def plot_hist_and_prob(data, label_data, neighbour=None):
    data_turn_right = data[label_data==1,...]
    if neighbour == 'All':
        data = np.reshape(data, [-1, 4])
        data_turn_right = np.reshape(data_turn_right, [-1, 4])
        title=str(neighbour) + 'neigbhours'
    else:
        data = data[:, neighbour, :]
        data_turn_right = data_turn_right[:, neighbour, :]
        title='neigbhour' + str(neighbour+1)

    h, e, range, bins = get_multi_histogram(data)
    h_r, e_r, range, bins = get_multi_histogram(data_turn_right, range=range, bins=bins)
    plt.ion()
    log_h=np.log10(h)
    log_h[np.isinf(log_h)] = np.nan
    vmin = np.nanmin(log_h)
    vmax = np.nanmax(log_h)
    log_h[np.isnan(log_h)] = vmin-1
    fig_positions_density = plot_multidim_histogram(log_h, e, bins, range, vmin, vmax,
                                                    cbar_label='log10(count)', title=title)

    p_r = h_r/h
    vmin = np.nanmin(p_r)
    vmax = np.nanmax(p_r)
    h[np.isnan(h)] = vmin-1
    fig_prob_turning = plot_multidim_histogram(p_r, e, bins, range, vmin, vmax,
                                                cbar_label='P(right)', title=title)
    return fig_positions_density, fig_prob_turning


def plot_data(datasets, num_neighbours):

    train_data = datasets.train.all['social']
    label_data = datasets.train.all['turn'][:, 0]
    social_and_focal = _social_and_focal_numpy(train_data, num_neighbours)
    data = restrict_variables_for_attention_numpy(social_and_focal) # v, xi, yi, vi

    figs_all_neighbours = plot_hist_and_prob(data, label_data, neighbour='All')

    figs_single_neighbours = [plot_hist_and_prob(data, label_data, neighbour=i)
                              for i in range(0, num_neighbours, 1)]

    return datasets, figs_all_neighbours, figs_single_neighbours


def save_figures(figures, save_folder, suffix='all'):
    figures[0].savefig(os.path.join(save_folder, "dist_positions_{}_neigh.pdf".format(suffix)), format='pdf')
    figures[1].savefig(os.path.join(save_folder, "prob_turning_{}_neigh.pdf".format(suffix)), format='pdf')


def plot_data_density_of_model(args):
    model, datasets = load_model_and_data(args.model_folder,
                                          new_constants_dict={'fixed_units': True,
                                                              'fixed_speeds': True})

    train_data, figs_all_neighbours, figs_single_neighbours = \
        plot_data(datasets, model.args["num_neighbours"])

    save_figures(figs_all_neighbours, args.model_folder)
    for i, figs in enumerate(figs_single_neighbours):
        save_figures(figs, args.model_folder, suffix=str(i+1))

    return train_data, model
