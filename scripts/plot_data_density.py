import argparse
from fishandra.plot.data_density import plot_data_density_of_model
import fishandra.loader_constants as loader_cons

# working

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_folder', type=str)
    parser.add_argument('-s', '--shuffle_data', action='store_true')
    parser.add_argument('-n', '--save_figures_per_neigbour', action='store_true')
    parser.set_defaults(shuffle_data=False)
    parser.set_defaults(save_figures_per_neigbour=False)
    args = parser.parse_args()

    if args.shuffle_data:
        loader_cons.SHUFFLE = 'trajectories'

    datasets, model = plot_data_density_of_model(args)
    return datasets, model


if __name__ == '__main__':
    datasets, model = main()
